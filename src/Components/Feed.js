import React from 'react';
import './Feed.css';
import './FeedOverlay.js';
import FeedOverlay from './FeedOverlay.js';
import Video from './Video.js';

export default function Feed({socket}) {
  return (
    <div className="Feed">
      <Video socket={socket}/>
      <FeedOverlay socket={socket}/>
    </div>
  );
}

