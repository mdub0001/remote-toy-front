import React, {useEffect} from 'react'; // {} for importing fuction
import Feed from './Feed.js';
import Controls from './Controls.js';
import io from 'socket.io-client';
import CatToday from '../img/TodaysCat.png';
import KeyboardArrowDown from '@material-ui/icons/KeyboardArrowDown';
import KeyboardArrowUp from '@material-ui/icons/KeyboardArrowUp';
import './App.css';


export default function App() {

  const socket = io('https://munieradubery.com/user', {
    path: '/play-with-cats-api/socket.io'
  });

  // a function that runs after first render
  // dependency list allows rendering only when changes observed
  // replaces: componentdidmount, componentdidupdate, componentwillupdate
  useEffect(()=> {
    socket.on('connect', () => {
      console.log("Connected");
    });
  }, [socket]);

  return (
    <div className="App">
      <header><h1>Play With Cats</h1></header>
        <div className="Content">
            <div className="Todaycats">
              <h2>Today's Cats</h2>
              <div class="arrowup">
                <KeyboardArrowUp/>
              </div>
              <h3>Bubbles</h3>
              <img src={CatToday} alt="Shelter cat"></img>
              <div>
                <ul>
                  <li>Grumpy</li>
                  <li>Greedy</li>
                </ul>
              </div>
              <div class="arrowdown">
                <KeyboardArrowDown/>
              </div>
          </div>
          <div className="Controller">
            <Feed socket={socket}/>
            <Controls socket={socket}/>
          </div>
          <div className="Shelterinfo">
            <h2>Shelter Information</h2>
            <p>this is a short section with information about the cat shelter you are currently playing in.</p>
            <p>Information could include; where they are based, the sort of cats they look after, and the number of cats they rehome per year.</p>
            <p>You may want to find out more about the shelter or donate, so their website link is found here:</p>
            <button>Shelter Website</button>
          </div>
        </div>
    </div>
  );
}

