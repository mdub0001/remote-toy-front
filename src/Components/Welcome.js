import React from "react";
import './Welcome.css';

export default function Welcome({handleJoin}) {

  return (
    <div className="Welcome">
        <strong>Welcome to play with cats, the site that lets you play with cats over the internet.</strong>
        <p>
          You can watch others play with cats through the video feed above, or play with the cats youself by joining the queue.<br/>
          Once you are at the front of the queue, you will have full control of the robot mouse using the arrow buttons that will appear here.<br/>
          When the timer runs out, you'll return to the back of the queue.
        </p>
        <strong>Click "Join Queue" to play with cats now!</strong>
        <br/><br/>
        <div><button onClick={handleJoin}>Join Queue</button></div>
    </div>
  );
}
