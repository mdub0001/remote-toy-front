import React, {useEffect, useState} from 'react';
import './Controls.css';
import Welcome from './Welcome.js';

// Pure component doesn't render if things change (controls enabled)
// props is deconstructed into just socket, like extracting a keyvalue
export default function Controls({socket}) {
  // useState is factory for [var, setter]
  const [controlsActive, setControlsActive] = useState(false); // returns a var and setter for the vars. Active = false.
  const [welcome, setWelcome] = useState(true); // useful for react to know when to rerender

  // didmount, once controls loaded on page
  // [] prevents calling on ever load/refresh
  useEffect(() => {
    document.onmouseup = () => socket.emit("stop");
    document.ontouchend = () => socket.emit("stop");
    socket.on("queue_position", (res) => {
      setControlsActive(res.queue_position === 0);  // if queue position 0, set active to true, else false
    });
  }, [socket]); 

  const handleMouseDown = (input) => {
    console.log(input);
    socket.emit(input);
  }

  const handleJoin = () => {
    console.log("join");
    socket.emit("queue");
    setWelcome(false);
  }
  
  const controls = (
    <div className="Controls">
      <div>
        <button className="forward" onMouseDown={() => handleMouseDown("forward")} onTouchStart={() => handleMouseDown("forward")} disabled={!controlsActive}><span></span></button>
      </div>
      <div className="leftright">
        <button className="left" onMouseDown={ () => handleMouseDown("left") } onTouchStart={() => handleMouseDown("left")} disabled={!controlsActive} ><span></span></button>
        <button className="right" onMouseDown={ () => handleMouseDown("right") } onTouchStart={() => handleMouseDown("right")} disabled={!controlsActive} ><span></span></button>
      </div>
      <div>
        <button className="backward" onMouseDown={ () => handleMouseDown("backward") } onTouchStart={() => handleMouseDown("backward")} disabled={!controlsActive} ><span></span></button>
      </div>
    </div>
  );

  // no () means function won't execute straight away. With params means must us () => func(param)
  return (
    welcome ? <Welcome handleJoin={handleJoin}/> : controls
  );
}


