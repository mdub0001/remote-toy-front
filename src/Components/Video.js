import React, {useEffect, useState} from 'react';
import './Video.css';
import OfflineImgOverhead from '../img/OfflineScreenOverhead.jpg';
import OfflineImgRat from '../img/OfflineScreenRat.jpg';

export default function Video({socket}) {

    const [cameraFrame, setCameraFrame] = useState(null);
    const [ratFrame, setRatFrame] = useState(null);
    const [mainVideo, setMainVideo] = useState(true);

    useEffect(() => {
        // socket listeners
        socket.on("camera_frame", (res) => {
            setCameraFrame(res.frame);
        });
        socket.on("rat_frame", (res) => {
            setRatFrame(res.frame);
        });
        socket.on("rat_disconnected", () => {
            setRatFrame(null); // will trigger offline message
        });
        socket.on("camera_disconnected", () => {
            setCameraFrame(null);
        });

    });

    const toggleVideos = () => {
        setMainVideo(!mainVideo); // inverts
    }

    return (
        <div className="Video">
            <img className={mainVideo ? 'mainvid' : 'cornervid'} onClick={() => toggleVideos()} src={cameraFrame ? "data:image/jpg;base64," + cameraFrame : OfflineImgOverhead} alt='Stream offline'/>
            <img className={!mainVideo ? 'mainvid' : 'cornervid'} onClick={() => toggleVideos()} src={ratFrame ? "data:image/jpg;base64," + ratFrame : OfflineImgRat} alt='Stream offline'/>
        </div>
    );
}
