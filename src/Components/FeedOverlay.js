import React, {useEffect, useState} from 'react';
import './FeedOverlay.css';

export default function FeedOverlay({socket}) {
    const [isLive, setIsLive] = useState(false);
    const [queueTimer, setQueueTimer] = useState(0);
    const [queuePosition, setQueuePosition] = useState(null);

    useEffect(() => {
        // socket listeners
        socket.on("active_timer", (res) => {
            setQueueTimer(res.active_timer)
        });
        socket.on("queue_position", (res) => {
            setQueuePosition(res.queue_position)
        });
        socket.on("rat_disconnected", () => {
            setIsLive(false); // will trigger offline message
        });
        socket.on("rat_frame", () => {
            if(!isLive) setIsLive(true);
        });
    }); 

    //@TODO: "Remaining needs to change from red to green once state is queuing>playing
    //@TODO: implement minutes seconds counter
    let inQueue;
    if (queuePosition > 0) {
        inQueue = <div className="queue">{queueTimer} You are {queuePosition} in the queue</div>
    } else if (queuePosition === 0) {
        inQueue = <div className="queue">You are live</div>
    } else {
        inQueue = ""
    }

    return (
        <div className="FeedOverlay">
            <div className="live">{isLive}</div>
            {inQueue}
        </div>
    )
}
