# Play With Cats
## What is Play With Cats?
Play with cats is a project with the goal of increasing cat shelter adoptions and donations.
Inspired by Twitch Plays Pokemon, users control a robot mouse toy over the internet to play with cats. The mouse can be controlled by individuals via the website, or by a huge number of users with twitch streaming integration.
The users can use the two camera streams (one from the robots perspective and one from overhead) to manipulate the toy and watch the cats reactions.

## This sounds cool! Can I see a working demo?
Sure! There was a test stream which can be viewed here: https://www.twitch.tv/videos/804899977?t=00h07m20s

And an informational video which can be viewed here: https://youtu.be/HWtyuBZOBvQ

## So what does this code do?
This code is for the frontend component of the project, running on React. The frontend displays the two video streams sandwiched between information about the cats you are playing with and the shelter they are currently based in.
On clicking the "join queue" button, the user is provided a control pad that they can use to control the rat when they reach the front of the queue.

## Is this site currently live?
Not at the moment! But I may set it up again some time in the future :)
